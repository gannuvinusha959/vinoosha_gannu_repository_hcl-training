package com.hcl.angular.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.angular.model.Book;

public interface BookRepo extends  JpaRepository<Book, Long>{

	List<Book> findByPublished(boolean published);
	
	List<Book> findByTitleContaining(String title);
}
