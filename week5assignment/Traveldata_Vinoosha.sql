create database Traveldata_Vinoosha;

use Traveldata_Vinoosha;

create table PASSENGER
 (Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);


create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,18600;

select * from price;
/* Qn 1 */
select Gender,COUNT(Gender) from PASSENGER WHERE distance>=600 GROUP BY Gender;

/*Qn 2* /
select min(price) from price WHERE Bus_Type = 'sleeper';

/*Qn 3 */
select *from passenger WHERE Passenger_name LIKE 's%';

/*Qn 4 */
select Passenger_name,pp.Boarding_City,pp.Destination_city,pp.Bus_Type,pr.Price from PASSENGER pp, PRICE pr WHERE pp.Distance = pr.Distance and pp.Bus_type = pr.Bus_type;

/*Qn 5 */
select pp.Passenger_name,pp.Boarding_city,pp.Destination_city, pp.Bus_type, pr.Price from PASSENGER pp, PRICE pr WHERE pp.Distance = 1000 and pp.Bus_type = 'Sitting' and pp.Distance = 1000 and pp.Bus_type = 'Sitting';

/*Qn 6 */
select DISTINCT pp.Passenger_name, pp.Boarding_city as Destination_city, pp.Destination_city as Boardng_city, pp.Bus_type, pr.Price from PASSENGER pp, price pr WHERE Passenger_name = 'Pallavi' and pp.Distance = pr.Distance;

/* Qn 7 */
select DISTINCT distance from PASSENGER order by Distance desc;

/* Qn 8 */
select Passenger_name, Distance * 100.0/ (select SUM(Distance) from PASSENGER)from PASSENGER group by Distance;

/*Qn 9*/
 create view passen as select * from PASSENGER WHERE Category = 'AC';SELECT * from passen;

/* Qn 10*/
 delimiter &&
   create procedure pass()
     begin
     select * from passenger where bus_type ='Sleeper';
     select count(passenger_name) as totalpass from passenger where bus_type ='Sleeper';
     End &&
    call pass(); 

/*Qn 11*/
select * from passenger ORDER BY passenger_name  LIMIT 5;
