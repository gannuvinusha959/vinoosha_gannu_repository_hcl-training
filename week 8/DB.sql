create database mvc;
use mvc;

create table book(id int primary key,name varchar(100),genere varchar(100),url varchar(1000));
insert into book values(1,'Beloved','Novel','https://d2e111jq13me73.cloudfront.net/sites/default/files/styles/product_image_aspect_switcher_170w/public/product-images/csm-book/3936-orig.jpg?itok=QF7SlhK8');
insert into book values(2, 'Animal Farm',' Narrative','https://images-na.ssl-images-amazon.com/images/I/61KPPB-34FL.jpg');
insert into book values(3,'Expectation',' Novel ','https://images-eu.ssl-images-amazon.com/images/I/51SdnqnbXvL._SX342_SY445_QL70_ML2_.jpg');
insert into book values(4,' Rainbow',' Novel ','https://images-na.ssl-images-amazon.com/images/I/61eNjpUL9fL.jpg');
insert into book values(5,' Stranger',' Crime Fiction ','https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1432692911l/15660._SX318_.jpg');

create table user(username varchar(100),password varchar(100));
insert into user values('Vinoosha','1');
insert into user values('Ali','2');  

create table register(username varchar(100),password varchar(100));

create table Favouritebook(id int,name varchar(100),genere varchar(100),url varchar(100));

create table Readlaterbook(id int,name varchar(100),genere varchar(100),url varchar(100));

