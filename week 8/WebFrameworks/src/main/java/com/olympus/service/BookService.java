package com.olympus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olympus.bean.Book;
import com.olympus.bean.User;
import com.olympus.dao.BookDao;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;
	
	public List<Book> getAllBook() {
		return bookDao.getAllBook();
	}
    
	public List<User>getAllUser() {
		return bookDao.getAllUser();
		
	}

}
