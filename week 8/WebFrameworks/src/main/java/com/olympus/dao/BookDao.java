package com.olympus.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import com.olympus.bean.Book;
import com.olympus.bean.User;


@Repository
public class BookDao {

		@Autowired
		JdbcTemplate jdbcTemplate;
		
		public List<Book> getAllBook() {
			return jdbcTemplate.query("select * from book", new BooksRowMapper());
		}
		
		
		public List<Book> findBookById(int pid) {
		
			return jdbcTemplate.query("select * from book where id=?",new Object[] {1},new BooksRowMapper());
		}
		public List<User>getAllUser(){
			return jdbcTemplate.query("select * from user where username like ? and password like ?", new Object[] {1},new  UserRowMapper());
			
		}
	}
	class BooksRowMapper implements RowMapper<Book>{
		
		@Override
		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			Book p = new Book();
			p.setId(rs.getInt(1));
			p.setName(rs.getString(2));
			p.setGenre(rs.getString(3));
			p.setUrl(rs.getString(4));
			return p;
		}
		
	}
	class UserRowMapper implements RowMapper<User>{

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User u=new User();
			u.setUsername(rs.getString(1));
			u.setPassword(rs.getString(2));
			return u;
			
		}

}
