import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
usersList:Array<User>=[];
  constructor(
    public userService:UserService
  ) { }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers(){
this.userService.getAllUsers().subscribe(
  res=>this.usersList=res,
  error=>console.log(error)
)

  }
  updateUser(){

  }
  deleteUser(){
    
  }
}
