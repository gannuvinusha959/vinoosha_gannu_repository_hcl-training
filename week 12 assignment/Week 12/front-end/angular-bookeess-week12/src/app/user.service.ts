import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from './user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public http:HttpClient
  ) { }

  userLogin(user:User):Observable<string>{
    return this.http.post("http://localhost:8282/user/userLogIn",user,{responseType:'text'})

  }

  userSignIn(user:User):Observable<string>{
  return this.http.post("http://localhost:8282/user/userSignIn",user,{responseType:'text'})
  }
  getAllUsers():Observable<User[]>{
    return this.http.get<User[]>("http://localhost:8181/userCurd/getAllUser")
      }
}
