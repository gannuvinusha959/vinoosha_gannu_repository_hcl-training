package com.greatlearning.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.greatlearning.bean.Login;
import com.greatlearning.dao.LoginDao;

/**
 * Servlet implementation class FavouriteBookController
 * @param <HttpServletRequest>
 * @param <HttpServletResponse>
 */
@WebServlet("/FavouriteBookController")
public class FavouriteBookController<HttpServletRequest, HttpServletResponse> extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public FavouriteBookController() {
    	super();
        // TODO Auto-generated constructor stub
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		((ServletResponse) response).getWriter().append("Served at: ").append(((javax.servlet.http.HttpServletRequest) request).getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String id = ((ServletRequest) request).getParameter("favrouitebookid");
		Login user = LoginController.currentUser;
		String name = user.getUser();
		String password = user.getPassword();
		
		try {
			LoginDao.addLiked(name, password, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		((ServletResponse) response).setContentType("text/html");
		PrintWriter pw = ((ServletResponse) response).getWriter();
		pw.println("<html><body><h1>Book added successfully to liked section</h1></body></html>");


	}

}

