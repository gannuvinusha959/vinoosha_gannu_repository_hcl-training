package com.greatlearning.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.bean.Login;
import com.greatlearning.dao.LoginDao;


/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Login currentUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String username = request.getParameter("username");
        String password = request.getParameter("password");
        Login loginBean = new Login();
      loginBean.setUser(username);
       loginBean.setPassword(password);

    	try {
    		if(LoginDao.validate(loginBean)) {
    			
    			HttpSession session = request.getSession();
	              session.setAttribute("username",username);
	             // response.sendRedirect("display.jsp");
    			//currentUser.setUser(username);
    		//currentUser.setPassword(password);
    		response.sendRedirect("UserDetails.jsp");
    			
    		}else {
    			HttpSession session = request.getSession();
    			//response.sendRedirect("invalid.jsp");
    			RequestDispatcher rd = request.getRequestDispatcher("invalid.jsp");
        		rd.include(request, response);
    			
    		}
    	} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
