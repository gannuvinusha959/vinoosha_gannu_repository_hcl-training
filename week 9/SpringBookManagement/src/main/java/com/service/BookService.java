package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.dao.BookDao;

@Service
public class BookService {

	@Autowired
	BookDao bookDao;

	public List<Book> getAllBook() {
		return bookDao.findAll();
	}

	public Optional<Book> getBookById(int bookId ) {
		return bookDao.findById(bookId);
	}
	public String storeBook(Book book) {

		if (bookDao.existsById(book.getBookId())) {
			return "Book id must be unique";
		} else {
			bookDao.save(book);
			return "Book " + book.getBookId() + " stored successfully";
		}
	}

	public String deleteBook(int bookId) {
		if (!bookDao.existsById(bookId)) {
			return "Book  " + bookId + " details not present";
		} else {
			bookDao.deleteById(bookId);
			return "Book " + bookId + " deleted successfully";
		}
	}

	public String updateBookName(Book book) {
		if (!bookDao.existsById(book.getBookId())) {
			return "Book details not present";
		} else {
			Book b = bookDao.getById(book.getBookId());
			b.setBookName(book.getBookName());
			bookDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}

	public String updateAuthor(Book book) {
		if (!bookDao.existsById(book.getBookId())) {
			return "Book details not present";
		} else {
			Book b = bookDao.getById(book.getBookId());
			b.setAuthor(book.getAuthor());
			bookDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}

	public String updateBookGenre(Book book) {
		if (!bookDao.existsById(book.getBookId())) {
			return "Book details not present";
		} else {
			Book b = bookDao.getById(book.getBookId());
			b.setBookGenre(book.getBookGenre());
			bookDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}

	public String updateBookPrice(Book book) {
		if (!bookDao.existsById(book.getBookId())) {
			return "Book details not present";
		} else {
			Book b = bookDao.getById(book.getBookId());
			b.setBookPrice(book.getBookPrice());
			bookDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}
	public String updateBookStock(Book book) {
		if (!bookDao.existsById(book.getBookId())) {
			return "Book details not present";
		} else {
			Book b = bookDao.getById(book.getBookId());
			b.setBookStock(book.getBookStock());
			bookDao.saveAndFlush(b);
			System.out.println(b);
			return "Book updated successfully";
		}
	}
}
