package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties.Admin;
import org.springframework.stereotype.Service;

import com.bean.AdminLogin;
import com.bean.User;
import com.dao.AdminDao;

@Service
public class AdminService {

	@Autowired
	AdminDao adminDao;
	
	public String storeAdminInfo(AdminLogin admin) {

		if (adminDao.existsById(admin.getEmailId())) {
			return "Admin id must be unique";
		} else {
			adminDao.save(admin);
			return "Admin " + admin.getEmailId() + " Sign In successfully";
		}
	}
	
	public String updateAdminPassword(AdminLogin admin) {
		if(!adminDao.existsById(admin.getEmailId())) {
			return "Admin "+ admin.getEmailId()+" details not present";
		}
		else {
			AdminLogin a = adminDao.getById(admin.getEmailId());
			a.setAdminPassword(admin.getAdminPassword());
			adminDao.saveAndFlush(a);
			return "User " + admin.getEmailId() + " updated successfully";
		}
		
	}
	
	public String login(AdminLogin admin) {
		if(!adminDao.existsById(admin.getEmailId())) {
			return "Admin "+ admin.getEmailId()+" details not present";
		}
		else {
			AdminLogin a = adminDao.getById(admin.getEmailId());
			if (a.getAdminPassword().equals(admin.getAdminPassword())) {
				return "Admin "+ admin.getEmailId()+" Login Succesful";
			}
			else {
				return "Admin "+ admin.getEmailId()+" Login Failed";
			}
		}
	}
	
	public String logout(String email ) {
		if(adminDao.existsById(email)) {
			return "Successfully Logout";
		}
		else {
			return "Failed to Logout";
		}
		
	}
}
