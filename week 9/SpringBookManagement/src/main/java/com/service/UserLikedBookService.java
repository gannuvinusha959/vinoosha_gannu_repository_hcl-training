package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.User;
import com.bean.UserLikedBook;
import com.dao.UserLikedBookDao;

@Service
public class UserLikedBookService {
	@Autowired
	UserLikedBookDao likedDao;

	public List<UserLikedBook> getAllLikedBook() {
		return likedDao.findAll();
	}
	
	public List<String> getLikedBook(String email) {
		
		return likedDao.getLikedBook(email);
	}
	
	public String storeLikeBook(UserLikedBook book) {
		if (likedDao.existsById(book.getKey())) {
			return "Book is present in Liked List";
		}
		else {
			likedDao.save(book);
			return "Book stored to Liked List";
			
		}
	}
	
	public String deleteLikeBook(UserLikedBook book) {
		if (!likedDao.existsById(book.getKey())) {
			return "Book is not present in Liked List";
		}
		else {
			likedDao.delete(book);
			return "Book deleted from Liked List";
		}
	}

}
