package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.CompositeKey;
import com.bean.UserLikedBook;


@Repository
public interface UserLikedBookDao extends JpaRepository<UserLikedBook, CompositeKey> {

	@Query("select bookName from UserLikedBook u where u.key.emailId=:email")
	public List<String> getLikedBook(@Param("email") String email);
}
