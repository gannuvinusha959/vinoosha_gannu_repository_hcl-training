package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.UserLikedBook;
import com.service.UserLikedBookService;

@RestController
@RequestMapping("/likedBook")
public class UserLikedBookController {

	@Autowired
	UserLikedBookService likedService;

	@GetMapping(value = "getAllLikedBook", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserLikedBook> getAllBooks() {
		return likedService.getAllLikedBook();
	}

	@PostMapping(value = "storeLikeBook", consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeLikeBook(@RequestBody UserLikedBook book) {
		return likedService.storeLikeBook(book);
	}

	@GetMapping(value = "getLikedBook/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> getLikedBook(@PathVariable("email") String email) {
		return likedService.getLikedBook(email);
	}

}
