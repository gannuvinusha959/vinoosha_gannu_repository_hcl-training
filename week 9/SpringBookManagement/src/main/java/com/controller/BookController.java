package com.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Book;
import com.service.BookService;

@RestController
@RequestMapping("/books")
public class BookController {

	@Autowired
	BookService bookService;
	
	@GetMapping(value = "getAllBook",
			    produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBook(){
		return bookService.getAllBook();
	}
	
	@GetMapping(value = "getBookById/{bookId}",
		    produces = MediaType.APPLICATION_JSON_VALUE)
public Optional<Book> getBookById(@PathVariable("bookId") int bookId){
	return bookService.getBookById(bookId);
}
	
	@PostMapping(value = "storeBook",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBook(@RequestBody Book book) {
		
				return bookService.storeBook(book);
	}
	
	
	@DeleteMapping(value = "deleteBook/{bookId}")
	public String deleteBook(@PathVariable("bookId") int bookId) {
					return bookService.deleteBook(bookId);
	}
	
	@PatchMapping(value = "updateBookName")
	public String updateBookName(@RequestBody Book book) {
					return bookService.updateBookName(book);
	}
	
	@PatchMapping(value = "updateAuthor")
	public String updateAuthor(@RequestBody Book book) {
					return bookService.updateAuthor(book);
	}
	
	@PatchMapping(value = "updateBookGenre")
	public String updateBookGenre(@RequestBody Book book) {
					return bookService.updateBookGenre(book);
	}
	@PatchMapping(value = "updateBookPrice")
	public String updateBookPrice(@RequestBody Book book) {
					return bookService.updateBookPrice(book);
	
	}
	@PatchMapping(value = "updateBookInStock")
	public String updateBookStock(@RequestBody Book book) {
					return bookService.updateBookStock(book);
	}
}
