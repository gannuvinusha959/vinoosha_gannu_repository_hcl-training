package com.bean;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class UserLikedBook {
	@EmbeddedId
	private CompositeKey key;
	private String bookName;
	
	public CompositeKey getKey() {
		return key;
	}
	public void setKey(CompositeKey key) {
		this.key = key;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	@Override
	public String toString() {
		return "UserLikedBook [key=" + key + ", bookName=" + bookName + "]";
	}
	
	
	
	
}
