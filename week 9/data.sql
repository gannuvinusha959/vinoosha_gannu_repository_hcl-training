create database book_Vinoosha;

use book_Vinoosha;

create table book(
book_id int primary key,
book_name varchar(100),
author varchar(100),
book_genre varchar(100),
book_price float,
book_stock int
);

drop table book;

create table admin_log_in(
email_id varchar(100) primary key,
admin_password varchar(100)
);

drop table admin_login;

create table User(
email_id varchar (100) primary key,
username varchar(100),
user_password varchar(100)
);

drop table User;

create table user_LikedBook(
email_id varchar (100), 
book_id int,
book_name varchar(100),
foreign key (book_id) references book(book_id),
foreign key (email_id) references user(email_Id),
primary key(email_id,book_id)
);
drop table user_LikedBook;
desc user_LikedBook;


select * from book;
select * from Admin_Login;
select * from User;
select * from user_LikedBook;
